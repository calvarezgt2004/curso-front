Layout:

	-Header
	-Nav
	-Section
	-Article
	-Aside
	-Footer

Enlaces:

	-a

Textos:

	-h1 , h2, h3, h4, h5, h6
	-p
	-span

Imagenes y videos:

	-img
	-svg
	-iframe
	-video

Formularios:

	-Form
	-input
	-label
	-button

Listas:

	-ul
	-li
	-ol

Pagina de ejemplos de etiquetas: https://htmlreference.io