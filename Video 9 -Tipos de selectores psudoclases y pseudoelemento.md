		PSUDOCLASES         |         PSUEDOELEMENTOS
		
		:activate           |         ::after
		:focus              |         ::before
		:hover              |         ::first-letter
		:nth-child(n)       |         ::placeholder


Enlaces de mas contenido:

https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-elements

https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-classes