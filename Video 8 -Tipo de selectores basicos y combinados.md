									Selectores basicos
									
			De Tipo               |    div {...}
			De clase              |   .elemento{...}
			De ID                 |   #id-del-elemnto{...}
			De atributo           |   a[href = "..."]{...}
			Universal             |   *{...}

Pagina para ver colores css:

https://htmlcolorcodes.com

									Selectores Combinados
									
			Descendiente              |    div p {...}
			Hijo directo              |    div > p {...}
			Elemento adyacente        |    div + p {...}
			General de hermanos       |    div ~ p
