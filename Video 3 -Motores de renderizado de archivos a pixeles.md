Tranforman el codigo que nosotros escribimos a pixeles y cosas entendibles que la computadra entienda

Algunos de los navegadores y motores que usan:

                           Navegador                        Motor
                           
                            Chrome                          Blink
                            Edge                           Edge HTML
                            Safari                          WebKit
                            Firefox                         Gecko


El navegador para transformar los archivos hace los siguientes pasos:

1- Pasa los archivos a objetos, esto lo hace con el DOM

2- Calcula los estilos correspondientes a cada node, con el DOM

3- Calcular las dimensiones de cada nodo y donde va en la pantalla

4- Pinta las diferentes cajas

5- Toma la capa y convierte en una imagen para mostrar en la patalla 

El DOM: Document object Model, Define la estructura lógica de los documentos y el modo en que se accede y manipula.