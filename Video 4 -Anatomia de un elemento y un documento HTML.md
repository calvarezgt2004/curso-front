Familiarizarse con las etiquetas

Etiqueta de apertura: <>

Contenido: "Hola mundo"

Etiqueta de cierre </>

Atributos: nos sirven para poder comunicarnos con el css o para identificar los elementos que esten dentro de el. 

Ejemplo: <h1 class="Title">
                          |           |
                   Atributo  Valor

Anidamiento: Unas etiquetas que estan dentro de otras:
<!-- 
<section>
	<h1> Plazti </h1>
	<p> Tiene una comunidad </p>
	<ul>
		<li> Increible </li>
		<li> Maravillosa </li>
		<li> Inigualable </li>
	</ul>
</section>
-->

Elementos que no tiene cierre:

<img src="" alt="">

Anatomia de un html:

<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
<!-- Aqui va el codigo -->
</body>

</html>

EJEMPLO DE UN ARCHIVO HTML CON OBJETOS:

<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Document</title>

</head>

<body>

    <section>

        <h1> Plazti </h1>

        <p> Tiene una comunidad </p>

        <ul>

            <li> Increible </li>

            <li> Maravillosa </li>

            <li> Inigualable </li>

        </ul>

    </section>

</body>

</html>
